///<reference types="@tom-konda/typedef-drupal-core/drupal" />

import { detachTrigger } from "@tom-konda/typedef-drupal-core/drupal"

type editorFormat = {
  editor: keyof typeof Drupal.editors,
  editorSettings: Record<string, unknown>,
  editorSupportsContentFiltering: boolean,
  format: keyof typeof drupalSettings.editor,
  isXssSafe: boolean,
}

export type editor = {
  attach: (element: HTMLElement, format: editorFormat) => unknown,
  detach: (element: HTMLElement, format: editorFormat, trigger?: detachTrigger) => unknown,
  onChange: (element: HTMLElement, callback: () => void) => unknown,
  attachInlineEditor?: (element: HTMLElement, format: editorFormat, mainToolbarId?: string, floatedToolbarId?: string) => boolean,
}

declare global {
  namespace drupalSettings {
    const editor: {
      formats: Record<string, editorFormat>,
    }
  }
  namespace Drupal {
    let editors: Record<string, editor>
    function editorAttach (field: HTMLElement, format: editorFormat): void
    function editorDetach (field: HTMLElement, format: editorFormat, trigger?: detachTrigger): void
  }
}
